# Create local Consul and Vault 

Simple bash script to create Consul and Vault using Minikube locally. 


## Requirements

- Install Minikube - https://minikube.sigs.k8s.io/docs/start/
- Install Helm - https://helm.sh/docs/intro/install/


Once Minikube is running you run the script 
```sh
bash start-vault.sh  
```
Once the script finishes, open a second terminal and port forward Vault to local host.

```sh
kubectl port-forward vault-0 8200:8200
```

Then set, in the first termianl, VAULT_ADDR to your local host for Vault and use the root token saved in root_token file to login to Vault in terminal or in the UI via the browser. 

```sh
export VAULT_ADDR='http://127.0.0.1:8200'
cat root_token
vault login
```